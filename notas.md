Exercícios do curso do Blau: https://codeberg.org/cursos-livres/CBPS

Blau, minhas anotações estão em um caderno aqui, tudo escrito a caneta mesmo hehehe...


### Questões da semana 01 respondidas:

## Entendeu? Então me explica...

Escolha as alternativas corretas (se houver alguma).

### 1. O que o shell não é?

- [ ] Uma interface.
- [ ] Uma linguagem de programação.
- [X] Um compilador de programas. 
- [ ] Um interpretador de comandos.

### 2. Qual é o shell padrão na maioria dos sistemas GNU/Linux?

- [ ] ash
- [ ] dash
- [X] bash 
- [ ] sh

### 3. Onde está o erro na pergunta anterior?

- [ ] Todos esses shells podem ser encontrados no GNU/Linux.
- [ ] O shell `sh` é do UNIX.
- [X] Depende do que você quer dizer com "padrão".
- [ ] Os shells `ash` e `dash` são POSIX.

### 4. O sistema operacional gerencia programas em execução...

- [ ] Através da memória.
- [ ] Pela linha de comandos.
- [X] Através de processos.
- [ ] Pelo programa `top`.

### 5. O período de execução do shell define...

- [ ] Um script.
- [ ] Um programa.
- [ ] Um processo.
- [X] Uma sessão.

### 6. Uma nova sessão do shell pode ser iniciada...

- [X] Pela abertura de um terminal.
- [X] Por *pipes* e agrupamentos com parêntesis.
- [X] Pela invocação do executável do shell.
- [X] Pela execução de um script.

### 7. No Bash, todos os comandos em uma pipeline...

- [ ] São executados na sessão mãe.
- [X] São executados em um subshell.
- [X] Recebem uma cópia do ambiente da sessão mãe.
- [ ] São encadeados condicionalmente.

### 8. Qual é a ordem correta de processamento de uma linha de comando?

1. Redirecionamentos. (4)
1. Separação da linha em palavras e operadores. (1)
1. Monitoramento do estado de saída. (5)
1. Classificação de comandos. (2)
1. Expansões. (3)

- [ ] 3 2 5 1 4
- [X] 2 4 5 1 3
- [ ] 1 2 3 5 4
- [ ] 4 2 1 3 5

### 9. Em que etapa de processamento os apelidos são expandidos?

- [ ] Terceira.
- [ ] Segunda.
- [X] Primeira.
- [ ] Quarta.

### 10. Quais operadores de controle não estão definidos corretamente?

- [ ] `&&` e `||`: encadeamento condicional.
- [ ] `|` e `|&`: encadeamento por pipe.
- [X] `&` e `;`: execução assíncrona (segundo plano).
- [X] `;` e `nova linha`: separadores de linhas de comando.

### 11. Quais são as afirmativas incorretas?

- [ ] Parâmetros são os dados associados a uma sessão do shell. C
- [ ] Variáveis são parâmetros identificados por *nomes válidos*.
- [ ] Nomes válidos podem ter letras da tabela ASCII, sublinhado e números. 
- [X] Somente o shell pode criar e alterar variáveis.

### 12. Quais dos nomes abaixo são inválidos para variáveis?

- [X] `endereço`
- [ ] `turma1`
- [ ] `_`
- [ ] `__`

### 13. No shell, variáveis podem ser acessadas...

- [ ] Por expansão.
- [ ] Por referência.
- [ ] Por valor.
- [X] Variáveis só podem ser expandidas ou referenciadas.

### 14. No shell, variáveis não são declaradas porque...

- [ ] Não precisa.
- [X] Todos os dados são do tipo indeterminado.
- [X] Tudo é *string*.
- [X] O shell não reserva regiões na memória em função do tipo.

### 15. Quais comandos do Bash não definem, alteram atributos e exibem variáveis?

- [ ] `declare`
- [ ] `typeset`
- [ ] `local`
- [ ] `export`

### 16. Quais são as afirmativas corretas?

- [X] Em princípio, todas as variáveis são globais na sessão.
- [X] O termo "ambiente" inclui todas as variáveis de uma sessão.
- [X] Variáveis definidas pelo comando `declare` em funções são locais.
- [ ] Variáveis são exportadas para a sessão mãe com o comando `export`.

### 17. Quantas palavras existem no comando abaixo?

```
:~$ echo 'Maria tinha' um carneirinho.
```

- [ ] 5
- [X] 4
- [ ] 3
- [ ] 2

### 18. Qual seria a saída do comando abaixo?

```
:~$ var=duas palavras
```

- [X] `bash: palavras: comando não encontrado`
- [ ] `bash: duas: comando não encontrado`
- [ ] `bash: var=duas: comando não encontrado`
- [ ] Nenhuma saída.

### 19. Como eu poderia definir a variável 'nome' para obter o resultado abaixo?

```
:~$ echo "Seja bem-vindo, $nome."
Seja bem-vindo, João da Silva.
```

- [X] `nome='João da Silva'`
- [X] `read nome`
- [ ] `nome=João da Silva`
- [X] `nome="$1"`

### 20. Explique o comportamento abaixo:

```
# Causa erro...
:~$ nome=João da Silva
bash: da: comando não encontrado

# Não causa erro...
:~$ pessoa='João da Silva'
:~$ nome=$pessoa
:~$
```

- [ ] Porque os apelidos são expandidos antes das variáveis.
- [X] Porque as aspas só são removidas ao fim das expansões.
- [ ] Mentira! Também daria o mesmo erro!
- [X] Porque as aspas removem o significado especial dos espaços.
