#!/usr/bin/env bash

# Como saber os alunos cadastrados? Listar os nomes com números
# Sensível à caixa de texto. Maiúsculas e minúsculas
# Sensível à acentuação.
# Falta de mensagens de erro.
# ---------------------------------
# SOLUÇÕES:
# Imprimir uma lista de alunos.
# Mensagens de erro e sair

declare -A alunos

alunos[joão]=8.5
alunos[maria]=9.0
alunos[antônio]=6.5
alunos[luis carlos]=9.5
#alunos[luis d\'carlos]=9.5   contrabarra para substituir aspas

declare -l id

#declare -u id    para mostrar tudo em maiúsculo

echo Lista de alunos
echo -------------------------------
printf '%s\n' "${!alunos[@]}" | sort
echo -------------------------------
read -p 'Digite o nome de um(a) aluno(a): ' id

[[ ${alunos[$id]} ]] || { 
    echo "Este nome não existe!"
    exit
    }
    
#ou agrupamento de comandos em 1 linha:
#[[ ${alunos[$id]} ]] || { echo "Este nome não existe!"; exit; }

echo "A nota de $id é ${alunos[$id]}"
echo -------------------------------
