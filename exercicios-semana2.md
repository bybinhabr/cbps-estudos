# Exercícios da semana 2

## Sugestões para melhor aproveitamento

- Se não criou ainda, crie um repositório no codeberg.org com o nome `cbps-estudos`.
- Nele, publique suas anotações, respostas e scritpts.
- Se já tiver criado o arquivo `notas.md`, renomeie-o para `notas1.md`.
- Em seguida, crie um novo arquivo chamado `notas2.md` para justificar e estender as respostas dos exercícios da semana 2.
- Se quiser, compartilhe o link e peça uma avaliação nas nossas issues.

## Entendeu? Então me explica...

Escolha as alternativas corretas (se houver alguma).

### 1. Uma variável é...

- [X] Um identificador associado a um dado.
- [X] Um dado que pode variar ao longo da sessão.
- [ ] Declarada por um comando de atribuição.
- [X] Um parâmetro identificado por um nome válido.

### 2. A sessão do shell pode receber parâmetros...

- [X] Pela passagem de argumentos na linha do comando.
- [X] Pela atribuição de valores a nome de variáveis.
- [X] Pelo comando interno `set`.
- [X] Pela exportação de variáveis.

### 3. Sobre as expansões de parâmetros, o que é correto afirmar?

- [ ] Devemos utilizar as chaves para proteger variáveis (ex: `${var}`).
- [X] As chaves são opcionais para variáveis escalares.
- [X] Chaves são obrigatórias se o caractere seguinte a uma expansão for válido na formação de identificadores de variáveis.
- [X] Expansões de vetores com a informação de subscritos exigem as chaves.

### 4. Sobre vetores, quais são as afirmativas corretas?

- [X] Vetores são identificadores associados a uma lista de palavras.
- [ ] Vetores indexados sempre recebem índices automaticamente.
- [X] Vetores associativos são aqueles que recebem strings como índices.
- [ ] Vetores associativos não podem receber índices numéricos.

### 5. Quais dos vetores abaixo possuem 3 elementos?

- [X] `vec=(a b 'c d')`
- [X] `var='a b c'; vec=($var)`
- [X] `declare -a vec=([33]=a [42]=b [0]=c)`
- [X] `vec=(Maria Antônio 'Luis Carlos')`

### 6. Sabendo que `vec` é um vetor, justifique a saída abaixo:

```
:~$ echo $vec

:~$
```

- [ ] `vec` não tem elementos.
- [ ] O primeiro elemento de `vec` não está definido.
- [X] Não existe um elemento de índice `0`.
- [ ] Você quer nos enganar, `vec` não é um vetor.

### 7. Explique a saída abaixo:

```
:~$ printf '%s\n' "${vec[@]}"
a
b
c d
e
```

- [X] A expansão com o subscrito `[@]` entre aspas matém a separação das palavras no vetor.
- [X] O especificador de formato `%s\n` inclui uma quebra de linha ao final de cada palavra expandida.
- [ ] Por padrão, o comando interno `printf` imprime cada argumento em uma linha.
- [X] A sequência `c d` foi definida no vetor como uma palavra só.

### 8. Sobre os subscritos especiais `[@]` e `[*]`, qual afirmação está incorreta?

- [X] Sem aspas, não há diferença entre os comportamentos.
- [X] Com aspas, `[*]` expande todos os elementos como uma palavra só.
- [ ] Com aspas, `[*]` une as palavras no vetor com um espaço.
- [X] Com aspas, `[@]` separa as palavras com quebras de linha.

### 9. Qual será a saída do comando `echo`?

```
:~$ declare -A a=([vw]=fusca [fiat]=147 [ford]=corcel)
:~$ echo ${a[@]}
```

- [ ] `147 fusca corcel`
- [ ] `fusca 147 corcel`
- [ ] `corcel fusca 147`
- [X] Em princípio, não há como prever.

### 10. Explique a saída abaixo:

```
:~$ IFS='+'
:~$ printf '%s\n' "${vec[*]}"
a+b+c d+e
```

- [ ] A expansão com o subscrito `[*]` entre aspas expande apenas os elementos como uma palavra.
- [X] O primeiro caractere na variável `IFS` será a "cola" dos elementos do vetor na formação de uma palavra.
- [ ] A expansão com `[*]` entre aspas não respeita a separação de palavras dos elementos do vetor.
- [ ] Você se esqueceu de digitar um `+` entre `c` e `d`.

## Bora praticar!

### Alunos e notas

- Com qualquer editor de textos planos, crie um arquivo com o nome `notas.sh` e o seguinte conteúdo:

```
#!/usr/bin/env bash

declare -A alunos

alunos[joão]=8.5
alunos[maria]=9.0
alunos[antônio]=6.5
alunos[luis carlos]=9.5

read -p 'Digite o nome de um(a) aluno(a): ' id

echo "A nota é ${alunos[$id]}"
```

- Torne-o executável e analise seu comportamento.
- Preste uma atenção especial nos principais problemas que poderíamos ter se o script fosse executado por alguém que não sabe como ele foi escrito.
- Pense numa solução equivalente sem o uso de vetores associativos.
- Torne esse scritp compatível com as normas POSIX. (não pode ter VETORES!)
